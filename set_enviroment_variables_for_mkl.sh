#!/bin/bash
##Variables taken from:http://diracprogram.org/doc/release-12/installation/mkl.html

###############################################################
################# Takes 51 sec for sensor_id=3@ Uziel
#export MKL_NUM_THREADS="4"
#export MKL_DYNAMIC="FALSE"
#export OMP_NUM_THREADS=1
######

###############################################################
################# Take 53 sec for sensor_id=3@ Uziel
#export MKL_NUM_THREADS=4
#export MKL_DOMAIN_NUM_THREADS="MKL_BLAS=4"
#export OMP_NUM_THREADS=1
#export MKL_DYNAMIC="FALSE"
#export OMP_DYNAMIC="FALSE"
#####

###############################################################
################# Takes 38 sec  sec for sensor_id=3@ Uziel
export MKL_NUM_THREADS="2"
export MKL_DOMAIN_NUM_THREADS="MKL_BLAS=4"
export OMP_NUM_THREADS="1"
export MKL_DYNAMIC="FALSE"
export OMP_DYNAMIC="FALSE"

###############################################################
################# Takes 39 sec  sec for sensor_id=3@ Uziel
#export MKL_NUM_THREADS="4"
#export MKL_DOMAIN_NUM_THREADS="MKL_BLAS=2"
#export OMP_NUM_THREADS="1"
#export MKL_DYNAMIC="FALSE"
#export OMP_DYNAMIC="FALSE"

###############################################################
################# Takes 38 sec  sec for sensor_id=3@ Uziel
#export MKL_NUM_THREADS="2"
#export MKL_DOMAIN_NUM_THREADS="MKL_BLAS=2"
#export OMP_NUM_THREADS="4"
#export MKL_DYNAMIC="FALSE"
#export OMP_DYNAMIC="FALSE"


###############################################################
################# Takes 53 sec  sec for sensor_id=3@ Uziel
#export MKL_NUM_THREADS="4"
#export MKL_DOMAIN_NUM_THREADS="MKL_BLAS=1"
#export OMP_NUM_THREADS="1"
#export MKL_DYNAMIC="FALSE"
#export OMP_DYNAMIC="FALSE"








